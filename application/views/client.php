<div class="container">
  <div class="row">
    <div class="col-md-6 col-lg-5 ml-auto d-flex align-items-center mt-4 mt-md-0">
      <h2 class="mb-4 mobile-mt-2">Project Kami</h2>

    </div>
    <div class="col-lg-6 col-12">
      <!-- <h3 class="mb-4">Klien : 3</h3> -->
      <div class="row">
        <div class="col-sm-6">
          <div class="counter">
            <i class="fa fa-coffee fa-2x"></i>
            <h2 class="timer count-title count-number" data-to="3" data-speed="1500"></h2>
            <p class="count-text ">Happy Clients</p>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="counter">
            <i class="fa fa-lightbulb-o fa-2x"></i>
            <h2 class="timer count-title count-number" data-to="5" data-speed="1500"></h2>
            <p class="count-text ">Project Complete</p>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
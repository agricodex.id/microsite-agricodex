<div class="container">
    <div class="row">

        <div class="col-lg-7 col-md-12 col-12 d-flex align-items-center">
            <div class="about-text">
                <h1 class="animated animated-text">
                    <span class="mr-2">404. Halaman yang dimaksud</span>
                    <div class="animated-info">
                        <span class="animated-item">tidak ditemukan</span>
                        <span class="animated-item">sudah dihapus</span>
                    </div>
                </h1>
                <p><strong>Kami berusaha untuk bantu mengoptimalkan usaha Anda di era digital</strong>
                </p>

            </div>
        </div>

        <div class="col-lg-5 col-md-12 col-12">
            <div class="about-image svg">
                <img src="<?php echo base_url() ?>assets/images/404error.webp" class="img-fluid" alt="svg image">
            </div>
        </div>

    </div>
</div>
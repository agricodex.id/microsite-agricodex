<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="id">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Agricodex Indonesia | Konsultan Digital & Jasa Pengembangan Website, Aplikasi Mobile, Integrasi Jaringan dan Internet Of Things. Serta melayani procurement perangkat perusahaan.">
  <meta name="author" content="Agricodex">

  <title>Agricodex Indonesia | Konsultan Digital & Jasa</title>
  <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/logo-white.webp" />
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154986065-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-154986065-1');
  </script>
  <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "WebSite",
      "name": "Agricodex Indonesia | Konsultan Digital & Jasa",
      "url": "https://agricodex.id",
      "potentialAction": {
        "@type": "SearchAction",
        "target": "{search_term_string}",
        "query-input": "required name=search_term_string"
      }
    }
  </script>

  <!-- Load File CSS -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/unicons.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/owl.theme.default.min.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/package/floating-whatsapp/floating-wpp.min.css">


  <!-- MAIN STYLE -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/tooplate-style.css">

</head>

<body>
  <?php $this->load->view('menu'); ?>
  <!-- ABOUT -->
  <section class="about full-screen d-lg-flex justify-content-center align-items-center" id="about">
    <?php $this->load->view('about'); ?>
  </section>

  <!-- PRODUCT -->
  <section class="project py-5" id="project">
    <?php $this->load->view('product'); ?>
  </section>

  <!-- FEATURES -->
  <section class="resume py-5 d-lg-flex justify-content-center align-items-center" id="resume">
    <?php $this->load->view('client'); ?>
  </section>

  <!-- CONTACT -->
  <section class="contact py-5" id="contact">
    <?php $this->load->view('contact'); ?>
  </section>

  <div class="floating-wpp"></div>

  <!-- FOOTER -->
  <?php $this->load->view('footer'); ?>

  <!-- Load File JS -->
  <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/Headroom.js"></script>
  <script src="<?php echo base_url() ?>assets/js/jQuery.headroom.js"></script>
  <script src="<?php echo base_url() ?>assets/js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/smoothscroll.js"></script>
  <script src="<?php echo base_url() ?>assets/js/custom.js"></script>
  <script src="<?php echo base_url() ?>assets/js/counter.js"></script>
  <script src="<?php echo base_url() ?>assets/package/floating-whatsapp/floating-wpp.min.js"></script>
</body>

<script>
  $(function() {
    $('.floating-wpp').floatingWhatsApp({
      phone: '+6285777558000',
      popupMessage: 'Ada yang perlu AgricodeX bantu?',
      showPopup: true,
      message: 'Hai AgricodeX, ',
      headerTitle: 'AgricodeX',
      position: 'right',
    });
  });
</script>
<script>
  function initMap() {
    var chicago = new google.maps.LatLng(-6.4986857, 106.7997478);

    var map = new google.maps.Map(document.getElementById('map'), {
      center: chicago,
      zoom: 17
    });

    var coordInfoWindow = new google.maps.InfoWindow();
    coordInfoWindow.setContent(createInfoWindowContent(chicago, map.getZoom()));
    coordInfoWindow.setPosition(chicago);
    coordInfoWindow.open(map);

    map.addListener('zoom_changed', function() {
      coordInfoWindow.setContent(createInfoWindowContent(chicago, map.getZoom()));
      coordInfoWindow.open(map);
    });
  }

  var TILE_SIZE = 256;

  function createInfoWindowContent(latLng, zoom) {
    var scale = 1 << zoom;

    var worldCoordinate = project(latLng);

    var pixelCoordinate = new google.maps.Point(
      Math.floor(worldCoordinate.x * scale),
      Math.floor(worldCoordinate.y * scale));

    var tileCoordinate = new google.maps.Point(
      Math.floor(worldCoordinate.x * scale / TILE_SIZE),
      Math.floor(worldCoordinate.y * scale / TILE_SIZE));

    return [
      'Agricodex Office',
      'Jl. Bojong Depok Baru: ',
      'KP KEDUNG JIWA - KAB. BOGOR'
    ].join('<br>');
  }

  // The mapping between latitude, longitude and pixels is defined by the web
  // mercator projection.
  function project(latLng) {
    var siny = Math.sin(latLng.lat() * Math.PI / 180);

    // Truncating to 0.9999 effectively limits latitude to 89.189. This is
    // about a third of a tile past the edge of the world tile.
    siny = Math.min(Math.max(siny, -0.9999), 0.9999);

    return new google.maps.Point(
      TILE_SIZE * (0.5 + latLng.lng() / 360),
      TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
  }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvzdLxz-HkR3KmHNOk_EgfIUBq1bhOb98&callback=initMap">
</script>
<script src="https://www.google.com/recaptcha/api.js?render=6Le1QMoUAAAAAASh_i5yhDkW0e6jAedvYuZIZSu8"></script>
<script>
  grecaptcha.ready(function() {
    grecaptcha.execute('6Le1QMoUAAAAAASh_i5yhDkW0e6jAedvYuZIZSu8', {
      action: 'homepage'
    });
  });
</script>

</html>
<div class="container">
  <div class="row">

    <div class="col-lg-5 mr-lg-5 col-12">
      <div class="google-map w-100">
        <div id="map" style="border:0;max-width:100%;height:450px;"></div>
        <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d700.7742336386373!2d106.80025950618068!3d-6.499174225086423!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c2314b73b091%3A0x4a1e9408e4326cc6!2sJl.%20Bojong%20Depok%20Baru%20No.97%2C%20Kedung%20Waringin%2C%20Kec.%20Bojong%20Gede%2C%20Bogor%2C%20Jawa%20Barat%2016923!5e0!3m2!1sen!2sid!4v1576336871977!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe> -->
      </div>

      <div class="contact-info d-flex justify-content-between align-items-center py-4 px-lg-5">
        <div class="contact-info-item">
          <h3 class="mb-3 text-white">Hubungi Kami</h3>
          <i class="fa fa-whatsapp" aria-hidden="true" style="color:white"></i>
          <a href="tel:+6285777558000" target="_blank">
            <span style="color:white"> +62 857 7755 8000</span></br>
          </a>
          <i class="fa fa-envelope-o" aria-hidden="true" style="color:white"></i>
          <a href="mailto:agricodex.id@gmail.com" target="_blank">
            <span style="color:white"> agricodex.id@gmail.com</span></br>
          </a>
          <i class="fa fa-instagram" aria-hidden="true" style="color:white"></i>
          <a href="https://www.instagram.com/agricodex.id/" target="_blank">
            <span style="color:white"> agricodex.id</span>
          </a>
          <!-- <p><a href="mailto:agricodex.id@gmail.com">agricodex.id@gmail.com</a></p> -->
        </div>

        <ul class="social-links">
          <!-- <li><a href="#" class="uil uil-dribbble" data-toggle="tooltip" data-placement="left" title="Dribbble"></a></li> -->
          <!-- <li><a href="https://www.instagram.com/agricodex.id/" target="_blank" class="uil uil-instagram" data-toggle="tooltip" data-placement="left" title="Instagram"></a></li> -->
          <!-- <li><a href="#" class="uil uil-youtube" data-toggle="tooltip" data-placement="left" title="Youtube"></a></li> -->
        </ul>
      </div>
    </div>
    <div class="col-md-6 col-lg-5 ml-auto d-flex align-items-center mt-4 mt-md-0">
      <h2 class="mb-4 mobile-mt-2">Kontak Kami</h2>

    </div>

    <!-- <div class="col-lg-6 col-12">
      <div class="contact-form">
        <h2 class="mb-4">Optimalkan Usaha Anda bersama AgricodeX</h2>
        <form action="" method="get">
          <div class="row">
            <div class="col-lg-6 col-12">
              <input type="text" class="form-control" name="name" placeholder="Your Name" id="name">
            </div>

            <div class="col-lg-6 col-12">
              <input type="email" class="form-control" name="email" placeholder="Email" id="email">
            </div>

            <div class="col-12">
              <textarea name="message" rows="6" class="form-control" id="message" placeholder="Message"></textarea>
            </div>

            <div class="ml-lg-auto col-lg-5 col-12">
              <input type="submit" class="form-control submit-btn" value="Send Button">
            </div>
          </div>
        </form>
      </div>
    </div> -->

  </div>
</div>
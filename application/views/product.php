<div class="container">
  <div class="row">
    <div class="col-lg-11 text-center mx-auto col-12">

      <div class="col-lg-8 mx-auto">
        <h2>Produk Kami</h2>
      </div>
      <br />

      <div class="owl-carousel owl-theme">
        <div class="item">
          <!-- software -->
          <div class="project-info">
            <div class="row">
              <div class="col-xs-6 col-sm-6">
                <img src="<?= base_url() ?>assets/images/software-development.webp" style="max-height: 70%;" alt="Slider">
              </div>
              <div class="col-xs-6 col-sm-6">
                <div class="inner">
                  <div class="row row-content">
                    <div class="col-md-12">
                      <div class="headline-wrap">
                        <div>
                          <h3><span class="reveal-text">Software Development</span></h3>
                        </div>
                        <div>
                          <p class="">Solusi bisnis yang dapat disesuaikan dalam pengembangan aplikasi seluler/Mobile, desktop, dan website.
                            Para ahli di Agricodex bisa Mengubah ide menjadi sebuah solusi yang tepat dengan teknologi terkini.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row row-cta">
                    <div class="col-md-12 cta-wrap">
                      <a href="#" class="cta-main"><span class="cta-text reveal-text">Pelajari Lebih Lanjut</span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- iot -->
        <div class="item">
          <div class="project-info">
            <div class="row">
              <div class="col-xs-6 col-sm-6">
                <img src="<?= base_url() ?>assets/images/iot.webp" style="max-height: 70%;" alt="Slider">
              </div>
              <div class="col-xs-6 col-sm-6">
                <div class="inner">
                  <div class="row row-content">
                    <div class="col-md-12">
                      <div class="headline-wrap">
                        <div>
                          <h3><span class="reveal-text">Internet of Things</span></h3>
                        </div>
                        <div>
                          <p class="">Agricodex Indonesia menawarkan beragam produk, layanan, dan pelatihan berkualitas yang disesuaikan dengan kebutuhan Otomasi Perangkat (Automatic Device), Sistem tertanam (embeded system), dan lainya.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row row-cta">
                    <div class="col-md-12 cta-wrap">
                      <a href="#" class="cta-main"><span class="cta-text reveal-text">Pelajari Lebih Lanjut</span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- network -->
        <div class="item">
          <div class="project-info">
            <div class="row">
              <div class="col-xs-6 col-sm-6">
                <img src="<?= base_url() ?>assets/images/network-integration.webp" style="max-height: 70%;" alt="Slider">
              </div>
              <div class="col-xs-6 col-sm-6">
                <div class="inner">
                  <div class="row row-content">
                    <div class="col-md-12">
                      <div class="headline-wrap">
                        <div>
                          <h3><span class="reveal-text">Network Integration</span></h3>
                        </div>
                        <div>
                          <p class="">Bekerjasama dengan beberapa penyedia layanan untuk memberikan layanan lengkap peralatan jaringan aktif / pemasangan kabel dalam infrastruktur jaringan berkecepatan tinggi serat, nirkabel, dan kabel untuk memenuhi permintaan yang semakin meningkat saat ini.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row row-cta">
                    <div class="col-md-12 cta-wrap">
                      <a href="#" class="cta-main"><span class="cta-text reveal-text">Pelajari Lebih Lanjut</span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- Procurement -->
        <!-- <div class="item">
          <div class="project-info">
            <div class="row">
              <div class="col-xs-6 col-sm-6">
                <img src="<?= base_url() ?>assets/images/business-procurement.webp" style="max-height: 70%;" alt="Slider">
              </div>
              <div class="col-xs-6 col-sm-6">
                <div class="inner">
                  <div class="row row-content">
                    <div class="col-md-12">
                      <div class="headline-wrap">
                        <div>
                          <h3><span class="reveal-text">Business Procurement</span></h3>
                        </div>
                        <div>
                          <p class="">Solusi bisnis yang dapat disesuaikan dalam pengembangan aplikasi seluler/Mobile, desktop, dan website.
                            Para ahli di Agricodex bisa Mengubah ide menjadi sebuah solusi yang tepat dengan teknologi terkini.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row row-cta">
                    <div class="col-md-12 cta-wrap">
                      <a href="#" class="cta-main"><span class="cta-text reveal-text">Pelajari Lebih Lanjut</span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div> -->
      </div>

    </div>
  </div>
</div>
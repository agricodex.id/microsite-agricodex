<div class="container">
    <div class="row">

        <div class="col-lg-7 col-md-12 col-12 d-flex align-items-center">
            <div class="about-text">
                <h1 class="animated animated-text">
                    <span class="mr-2">Agricodex menyediakan</span>
                    <div class="animated-info">
                        <span class="animated-item">Software Development</span>
                        <span class="animated-item">Business Procurement</span>
                        <span class="animated-item">IoT & Network Integration</span>
                    </div>
                </h1>
                <p>
                    AgricodeX adalah salah satu perusahaan di Indonesia yang bergerak dibidang teknologi informasi dan digital.
                    Dan kami siap membantu Anda untuk mempersiapkan segala keperluan perangkat IT baik untuk skala besar maupun kecil.</br>
                    <strong>Kami berusaha untuk bantu mengoptimalkan usaha Anda di era digital</strong>
                </p>
            </div>
        </div>

        <div class="col-lg-5 col-md-12 col-12">
            <div class="about-image svg">
                <img src="<?php echo base_url() ?>assets/images/1-main-image.webp" class="img-fluid" alt="svg image">
            </div>
        </div>

    </div>
</div>
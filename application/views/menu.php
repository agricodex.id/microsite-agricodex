<!-- MENU -->
<nav class="navbar navbar-expand-sm navbar-light">
    <div class="container">
        <a class="navbar-brand" href="<?=base_url()?>">
          <img src="<?php echo base_url() ?>assets/images/logo.webp" class="img-brand image-responsive" style="max-width:100%;height:110px;">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            <span class="navbar-toggler-icon"></span>
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <!-- <ul class="navbar-nav mx-auto">
                <li class="nav-item">
                    <a href="#about" class="nav-link"><span data-hover="Tentang">Tentang</span></a>
                </li>
                <li class="nav-item">
                    <a href="#project" class="nav-link"><span data-hover="Produk">Produk</span></a>
                </li>
                <li class="nav-item">
                    <a href="#resume" class="nav-link"><span data-hover="Projek">Projek</span></a>
                </li>
                <li class="nav-item">
                    <a href="#contact" class="nav-link"><span data-hover="Kontak">Kontak</span></a>
                </li>
            </ul> -->

            <ul class="navbar-nav ml-lg-auto">
            <li class="nav-item">
                    <a href="#about" class="nav-link"><span data-hover="Tentang">Tentang</span></a>
                </li>
                <li class="nav-item">
                    <a href="#project" class="nav-link"><span data-hover="Produk">Produk</span></a>
                </li>
                <li class="nav-item">
                    <a href="#resume" class="nav-link"><span data-hover="Projek">Projek</span></a>
                </li>
                <li class="nav-item">
                    <a href="#contact" class="nav-link"><span data-hover="Kontak">Kontak</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>

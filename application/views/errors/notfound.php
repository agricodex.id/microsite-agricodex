<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Agricodex Indonesia | Konsultan Digital & Jasa Pengembangan Website, Aplikasi Mobile, Integrasi Jaringan dan Internet Of Things. Serta melayani procurement perangkat perusahaan.">
    <meta name="author" content="Agricodex">

    <title>Agricodex Indonesia | Konsultan Digital & Jasa</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/logo-white.webp" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154986065-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-154986065-1');
    </script>
    <script type="application/ld+json">
        {
            "@context": "https://schema.org/",
            "@type": "WebSite",
            "name": "Agricodex Indonesia | Konsultan Digital & Jasa",
            "url": "https://agricodex.id",
            "potentialAction": {
                "@type": "SearchAction",
                "target": "{search_term_string}",
                "query-input": "required name=search_term_string"
            }
        }
    </script>

    <!-- Load File CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/unicons.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/owl.theme.default.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/package/floating-whatsapp/floating-wpp.min.css">


    <!-- MAIN STYLE -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/tooplate-style.css">

</head>

<body>
    <!-- MENU -->
    <nav class="navbar navbar-expand-sm navbar-light">
        <div class="container">
            <a class="navbar-brand" href="<?= base_url() ?>">
                <img src="<?php echo base_url() ?>assets/images/logo.webp" class="img-brand image-responsive" style="max-width:100%;height:110px;">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav">
                <!-- <ul class="navbar-nav mx-auto">
                <li class="nav-item">
                    <a href="#about" class="nav-link"><span data-hover="Tentang">Tentang</span></a>
                </li>
                <li class="nav-item">
                    <a href="#project" class="nav-link"><span data-hover="Produk">Produk</span></a>
                </li>
                <li class="nav-item">
                    <a href="#resume" class="nav-link"><span data-hover="Projek">Projek</span></a>
                </li>
                <li class="nav-item">
                    <a href="#contact" class="nav-link"><span data-hover="Kontak">Kontak</span></a>
                </li>
            </ul> -->

                <ul class="navbar-nav ml-lg-auto">
                    <li class="nav-item">
                        <a href="<?= base_url() ?>" class="nav-link"><span data-hover="Kembali">Kembali</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- ABOUT -->
    <section class="about full-screen d-lg-flex justify-content-center align-items-center" id="about">
        <?php $this->load->view('404'); ?>
    </section>

    <div class="floating-wpp"></div>

    <!-- FOOTER -->
    <?php $this->load->view('footer'); ?>

    <!-- Load File JS -->
    <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/Headroom.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jQuery.headroom.js"></script>
    <script src="<?php echo base_url() ?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/smoothscroll.js"></script>
    <script src="<?php echo base_url() ?>assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>assets/package/floating-whatsapp/floating-wpp.min.js"></script>
</body>

<script>
    $(function() {
        $('.floating-wpp').floatingWhatsApp({
            phone: '+6285777558000',
            popupMessage: 'Ada yang perlu AgricodeX bantu?',
            showPopup: true,
            message: 'Hai AgricodeX, ',
            headerTitle: 'AgricodeX',
            position: 'right',
        });
    });
</script>

<script src="https://www.google.com/recaptcha/api.js?render=6Le1QMoUAAAAAASh_i5yhDkW0e6jAedvYuZIZSu8"></script>
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6Le1QMoUAAAAAASh_i5yhDkW0e6jAedvYuZIZSu8', {
            action: 'homepage'
        });
    });
</script>

</html>